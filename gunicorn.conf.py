# On heroku when running in a container runtime, the X_FORWARDED_PROTO
# header is not sent from localhost. By default, gunicorn only allows
# X_FORWARDED_PROTO (and related) headers from localhost, so gunicorn
# ignores those headers.
#
# Setting this to '*' is not ideal, but it _does_ make the app work
# on heroku.
#
# We may be able to refine this later to a specific subnet, e.g.
# 169.254.* (one of the common container subnets).
forwarded_allow_ips = "*"
