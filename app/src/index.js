import React from 'react';
import { render } from 'react-dom'

import './index.css';

import App from './components/App';
import { Provider } from 'react-redux';
import store from './store/configureStore';
// import * as serviceWorker from './serviceWorker';

// import 'bootstrap/dist/css/bootstrap.min.css';

render(
  <Provider store={store}>
      <App/>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
// ServiceWorker.register();
