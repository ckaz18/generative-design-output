

// Default to first iteration
const userIterationState = {
    iteration: 1,
    error: '',
};

const lastUserIteration = {
    iteration: 1,
    error: '',
};

export const userIterationReducer = (state = userIterationState, action) => {
    switch (action.type) {
        case 'CURRENT_ITERATION':
            return {
                ...state,
                iteration: action.iteration || state.iteration,
            }
        case 'ITERATION_FAILED':
            return {
                ...state,
                error: action.payload,
            };
        default:
            return state;
    }
};


export const lastUserIterationReducer = (state = lastUserIteration, action) => {
    switch (action.type) {
        case 'UPDATE_LAST_ITERATION':
            return {
                ...state,
                iteration: action.iteration || lastUserIteration.iteration,
            }
        default:
            return state;
    }
}