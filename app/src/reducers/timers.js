const timerState = {
  hasBegun: false,
  error: '',
};

export const startTimerReducer = (state = timerState, action) => {
  switch (action.type) {
    case 'TIMER_BEGIN_TEST':
      return {
        ...state,
        hasFinished: action.hasFinished,
        hasBegun: action.hasBegun,
      };
    case 'TIMER_STOP_TEST':
      return {
        ...state,
        hasFinished: action.hasFinished,
        hasBegun: action.hasBegun,
      };
    default:
      return state;
  }
};
