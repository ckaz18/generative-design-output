import { combineReducers } from 'redux';
import { authReducer } from './auth';
import { startTimerReducer } from './timers';
import { userIterationReducer, lastUserIterationReducer } from './userIteration';


const rootReducer = combineReducers({
  signIn: authReducer,
  timer: startTimerReducer,
  userIteration: userIterationReducer,
  lastIteration: lastUserIterationReducer,
});

export default rootReducer;
