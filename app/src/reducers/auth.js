const authState = {
  error: '',
  token: null,
  userId: null,
};

export const authReducer = (state = authState, action) => {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        token: action.token,
        userId: action.userId,
      };
    case 'LOGIN_FAILED':
      return {
        ...state,
        error: action.payload,
      };
    case 'LOGOUT':
      return {
        ...authState,
        error: action.payload
      }
    default:
      return state;
  }
};
