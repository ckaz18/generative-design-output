

const config = {
  baseUrl: 'https://jsonplaceholder.typicode.com/',
  endPoints: {
    signIn: 'signIn',
    users: 'users'
  }
};

export default config;