import { useState } from 'react';
import store from '../store/configureStore';


export default function useUserCredentials() {
  const getToken = () => {
    return sessionStorage.getItem('token');
  };
  const getUserId = () => {
    return sessionStorage.getItem('userId');
  };


  const [token, setToken] = useState(getToken());
  const [userId, setUserId] = useState(getUserId());


  const saveToken = (userToken) => {
    sessionStorage.setItem('token', userToken);
    setToken(userToken);
  };

  const saveUserId = (userId) => {
    sessionStorage.setItem('userId', userId);
    setUserId(userId);
  };

  store.dispatch({
    type: 'LOGIN_SUCCESS',
    token: token,
    userId: userId,
  });

  return {
    setToken: saveToken,
    setUserId: saveUserId,
    token,
    userId,
  };
}
