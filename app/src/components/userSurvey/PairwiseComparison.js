import React from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import ComparisonButton from './ComparisonButton';
import store from '../../store/configureStore';

const PairwiseComparison = () => {
    window.scrollTo(0, 0);
    
    const token = useSelector((state) => state.signIn.token);
    const userId = useSelector((state) => state.signIn.userId );
    const stateIteration = useSelector((state) => state.userIteration.iteration);
    const history = useHistory();
    let comparisonResponses = [];

    const isUserFinished = () => {
        if (comparisonResponses.length < 15) {
            return false;
        }
        return true;
    }

    const onComparisonSelect = (options) => {
        const isFound = comparisonResponses.findIndex(response => response.question === options.question);
        if (isFound > -1) {
            options.target.parentNode.getElementsByTagName('button')[options.value === 1 ? 0 : 1].style.background = null;
            options.target.style.background = 'gray';
            comparisonResponses[isFound].response = options.value;
        } else {
            options.target.style.background = 'gray';
            comparisonResponses.push({
                question: options.question,
                response: options.value
            });
        }
    };

    const submit = () => {
        if (!isUserFinished()) {
            alert('Please answer all questions before proceeding.');
        } else {
            fetch('/pairwise-comparison', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    comparison: comparisonResponses.sort((a, b) => a.question - b.question),
                    iterationId: stateIteration,
                    taskNumber: 'pairwise-comparison-survey',
                    token,
                    userId,
                }),
            }).catch((error) => {
                history.push('/error-page', { error, message: 'Error attempting to store user selections for user survey.' })
            }).finally(() => {
                console.log("clear storage");
                sessionStorage.clear();
                console.log('get token post clear', sessionStorage.getItem('token'));
                store.dispatch({ type: 'LOGOUT', token: null, userId: null });

                setTimeout(() => {
                    console.log("Delayed for 1 second.");
                }, 1000);

                history.push('/signin');
            });
        }
    };

    const comparisonButtons = (
        <div>
            <h3>Pairwise Comparisons</h3>
            Select the scale title of each pair that represents the more significant source of workload for all of the design selection tasks in general.
            <div style={{
                display: 'table', alignItems: 'center'
            }}>
                <ComparisonButton label1='Mental Demand' label2='Physical Demand' question='1' onClick={onComparisonSelect} />
                <ComparisonButton label1='Temporal Demand' label2='Performance' question='2' onClick={onComparisonSelect} />
                <ComparisonButton label1='Effort' label2='Frustration' question='3' onClick={onComparisonSelect} />
                <ComparisonButton label1='Mental Demand' label2='Temporal Demand' question='4' onClick={onComparisonSelect} />
            </div>

            {/* Set 2 */}
            <div style={{
                display: 'table', alignItems: 'center'
            }}>
                <ComparisonButton label1='Effort' label2='Physical Demand' question='5' onClick={onComparisonSelect} />
                <ComparisonButton label1='Performance' label2='Frustration' question='6' onClick={onComparisonSelect} />
                <ComparisonButton label1='Effort' label2='Mental Demand' question='7' onClick={onComparisonSelect} />
                <ComparisonButton label1='Temporal Demand' label2='Frustration' question='8' onClick={onComparisonSelect} />
            </div>

            {/* Set 3 */}
            <div style={{ display: 'table', alignItems: 'center' }}>
                <ComparisonButton label1='Physical Demand' label2='Performance' question='9' onClick={onComparisonSelect} />
                <ComparisonButton label1='Mental Demand' label2='Performance' question='10' onClick={onComparisonSelect} />
                <ComparisonButton label1='Temporal Demand' label2='Effort' question='11' onClick={onComparisonSelect} />
                <ComparisonButton label1='Frustration' label2='Physical Demand' question='12' onClick={onComparisonSelect} />
            </div>

            {/* Set 4 */}
            <div style={{ display: 'table', alignItems: 'center' }}>
                <ComparisonButton label1='Frustration' label2='Mental Demand' question='13' onClick={onComparisonSelect} />
                <ComparisonButton label1='Physical Demand' label2='Performance' question='14' onClick={onComparisonSelect} />
                <ComparisonButton label1='Temporal Demand' label2='Effort' question='15' onClick={onComparisonSelect} />
            </div>
        </div>
    );

    return (
        <div>
            {comparisonButtons}
            <br /><br />
            <button className='button' style={{ width: '100px' }} onClick={() => submit()} >
                Finish Study
            </button>
        </div>

    );
};

export default PairwiseComparison;