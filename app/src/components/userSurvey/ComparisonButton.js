import React from 'react';
import PropTypes from 'prop-types';


const ComparisonButton = ({ onClick, label1 = '', label2 = '', question = 0 }) => {

    return (
        <div style={{ widthMax: '33.3%', marginLeft: '20rem', marginBottom: '3rem', marginTop: '1rem' }} id={`parentQuestion${question}`}>
            <button className='comparison-button' style={{ display: 'block', marginBottom: '5px' }} id={label1} onClick={(e) => {
                e.value = 0; e.question = question; onClick(e)
            }}>{label1}</button>
            <span style={{ marginLeft: '40%' }}>OR</span>
            <button className='comparison-button' style={{ display: 'block', marginTop: '5px' }} id={label2} onClick={(e) => {
                e.value = 1; e.question = question; onClick(e)
            }}>{label2}</button>
        </div>
    );
};

ComparisonButton.propTypes = {
 onClick: PropTypes.func,
 label1: PropTypes.any,
 label2: PropTypes.any,
 question: PropTypes.number
};

export default ComparisonButton;