import React from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import store from '../../store/configureStore';

const VisualDissimilaritySurvey = () => {
    const stateIteration = useSelector((state) => state.userIteration.iteration);
    const token = useSelector((state) => state.signIn.token);
    const userId = useSelector((state) => state.signIn.userId);
    const history = useHistory();

    const isUserFinished = () => {
        if (answers.findIndex((answer) => answer.response === 0) > -1) {
            return false;
        }
        return true;
    }

    const next = () => {
        if (!isUserFinished()) {
            alert('Please answer all questions before proceeding.');
        } else {
            fetch('/user-survey-visual', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    iterationId: stateIteration,
                    taskNumber: 'user-survey-visual',
                    tlx: answers,
                    token,
                    userId,
                }),
            }).catch((error) => {
                history.push('/error-page', { error, message: 'Error attempting to store user comparison results.' })
            });

            if (stateIteration === 7) {
                history.push('/pairwise-comparison-survey');
            } else {
                store.dispatch({ type: 'UPDATE_LAST_ITERATION', iteration: stateIteration });
                console.log(`UPDATE_LAST_ITERATION: ${stateIteration}`);
                history.push('/transition');
            }
        }
    };

    const marksHighLow = {
        '0': 'Very Low',
        '5': '5',
        '10': '10',
        '15': '15',
        '20': '20',
        '25': '25',
        '30': '30',
        '35': '35',
        '40': '40',
        '45': '45',
        '50': '50',
        '55': '55',
        '60': '60',
        '65': '65',
        '70': '70',
        '75': '75',
        '80': '80',
        '85': '85',
        '90': '90',
        '95': '95',
        '100': 'Very High',
    };

    const marksExcellentPoor = {
        '0': 'Poor',
        '5': '5',
        '10': '10',
        '15': '15',
        '20': '20',
        '25': '25',
        '30': '30',
        '35': '35',
        '40': '40',
        '45': '45',
        '50': '50',
        '55': '55',
        '60': '60',
        '65': '65',
        '70': '70',
        '75': '75',
        '80': '80',
        '85': '85',
        '90': '90',
        '95': '95',
        '100': 'Excellent',
    };

    let answers = [{
        type: null,
        question: 1,
        response: -1
    }, {
        type: null,
        question: 2,
        response: -1
    }, {
        type: null,
        question: 3,
        response: -1
    }, {
        type: 'comparison',
        question: 4,
        response: -1,
    }, {
        type: 'comparison',
        question: 5,
        response: -1,
    }, {
        type: 'comparison',
        question: 6,
        response: -1,
    }, {
        type: 'comparison',
        question: 7,
        response: -1,
    }, {
        type: 'comparison',
        question: 8,
        response: -1,
    }, {
        type: 'comparison',
        question: 9,
        response: -1,
    }];

    const onSliderChange = (sliderOptions) => {
        const { value, tabIndex } = sliderOptions;

        if (value && tabIndex > -1 && answers[tabIndex - 1]) {
            answers[tabIndex - 1].response = value;
        }
    };

  const beginningQuestions = [{
    id: 1,
    title: 'generatedModelSatisfaction',
    question: `I am confident that the design alternative I selected is the most visually different.`
  }];

    const questionsDesign = [{
        title: 'Mental Demand',
        question: `How mentally demanding was the visual dissimilarity task during Iteration ${stateIteration}?`,
        id: 4
    }, {
        title: 'Physical Demand',
        question: `How physically demanding was the visual dissimilarity task during Iteration ${stateIteration}?`,
        id: 5
    }, {
        title: 'Temporal Demand',
        question: `How hurried or rushed was the pace of the visual dissimilarity task during Iteration ${stateIteration}?`,
        id: 6
    }, {
        title: 'Performance',
        question: `How successful were you in accomplishing the visual dissimilarity task during Iteration ${stateIteration}?`,
        id: 7
    }, {
        title: 'Effort',
        question: `How hard did you have to work to accomplish your level of performance on the visual dissimilarity task during Iteration ${stateIteration}?`,
        id: 8
    }, {
        title: 'Frustration',
        question: `How insecure, discouraged, irritated, stressed, and annoyed were during the visual dissimilarity task during Iteration ${stateIteration}?`,
        id: 9
    }];

  const radioOptions = [{
    value: 1,
    name: 'Strongly Disagree'
  },{
    value: 2,
    name: 'Disagree'
  },{
    value: 3,
    name: 'Neutral'
  },{
    value: 4,
    name: 'Agree'
  },{
    value: 5,
    name: 'Strongly Agree'
  }];
  
  const onRadioCheck = (radioOptions) => {
    const { target } = radioOptions;
    
    if (target.value) {
      const radioIndexRes = beginningQuestions.findIndex(item => item.title === target.name);
      answers[radioIndexRes].response = parseInt(target.value);
      answers[radioIndexRes].type = target.name;
      answers[radioIndexRes].question = radioIndexRes + 1;
    }
  };

  const radioQuestions = (
    <div>
      <h4>For each of the three questions below, please provide a rating response.</h4>
      <form>
        {beginningQuestions.map((item) => {
          return (
            <div style={{ paddingBottom: "5px", margin: "5px" }}>
              <p>
                {item.question}
              </p>
                {radioOptions.map((input, radioIdx) => {
                  const labelId = `${item.title}-${radioIdx}-radio`;
                    return (
                      <label style={{ marginRight: "5px", marginLeft:"5px" }}>
                        <input type="radio" id={labelId} name={item.title} value={input.value} onChange={onRadioCheck} />
                        {input.name}
                      </label>
                    );
                }) || null }
          </div>
           );
          }) || null }
      </form>
    </div>
  );


    const questionaire = (
        <div style={{ marginTop: '30px' }}>
            {questionsDesign.map((question, idx) => {
                let config = {
                    marks: marksHighLow,
                    reverse: false,
                    defaultValue: 0,
                    max: 100,
                    min: 0,
                    step: 5,
                };
                if (question.title === 'Performance') {
                    config.marks = marksExcellentPoor;
                }

                return (
                    <div>
                        {idx === 0 ? <h2>Visual Dissimilarity Task</h2> : null}
                        <div key={idx} style={{height: '10rem', width: '55rem'}}>

                            <div style={{ marginTop: '1rem' }}>
                                {(idx + 1)}. <b>{question.title}:</b> {question.question}
                            </div>
                            <br />
                            {/* <strong style={{ marginLeft: '46%' }}>{question.title}</strong> */}
                            <div style={{ marginLeft: '2rem', marginTop: '1rem' }}>

                                <Slider defaultValue={config.defaultValue} marks={config.marks} max={config.max} min={config.min} step={1} reverse={config.reverse}
                                    handle={onSliderChange} tabIndex={question.id} index={idx}
                                ></Slider>

                            </div>
                        </div>
                    </div>
                )
            })}
        </div >
    );

    // const comparisonButtons = (
    //     <div>
    //         <h3>Pairwise Comparisons</h3>
    //         Select the scale title of each pair that represents the more significant source of workload for all of the visual dissimilarity tasks in general.
    //         <div style={{ margin: '5px', display: 'table', alignItems: 'center' }}>
    //             <ComparisonButton label1='Mental Demand' label2='Physical Demand' question='1' onClick={onComparisonSelect} />
    //             <ComparisonButton label1='Temporal Demand' label2='Performance' question='2' onClick={onComparisonSelect} />
    //             <ComparisonButton label1='Effort' label2='Frustration' question='3' onClick={onComparisonSelect} />
    //             <ComparisonButton label1='Mental Demand' label2='Temporal Demand' question='4' onClick={onComparisonSelect} />
    //         </div>

    //         {/* Set 2 */}
    //         <div style={{ margin: '5px', marginTop: '0.5rem', display: 'table', alignItems: 'center' }}>
    //             <ComparisonButton label1='Effort' label2='Physical Demand' question='5' onClick={onComparisonSelect} />
    //             <ComparisonButton label1='Performance' label2='Frustration' question='6' onClick={onComparisonSelect} />
    //             <ComparisonButton label1='Effort' label2='Mental Demand' question='7' onClick={onComparisonSelect} />
    //             <ComparisonButton label1='Temporal Demand' label2='Frustration' question='8' onClick={onComparisonSelect} />
    //         </div>

    //         {/* Set 3 */}
    //         <div style={{ margin: '5px', marginTop: '0.5rem', display: 'table', alignItems: 'center' }}>
    //             <ComparisonButton label1='Physical Demand' label2='Performance' question='9' onClick={onComparisonSelect} />
    //             <ComparisonButton label1='Mental Demand' label2='Performance' question='10' onClick={onComparisonSelect} />
    //             <ComparisonButton label1='Temporal Demand' label2='Effort' question='11' onClick={onComparisonSelect} />
    //             <ComparisonButton label1='Frustration' label2='Physical Demand' question='12' onClick={onComparisonSelect} />
    //         </div>

    //         {/* Set 4 */}
    //         <div style={{ margin: '5px', marginTop: '0.5rem', display: 'table', alignItems: 'center' }}>
    //             <ComparisonButton label1='Frustration' label2='Mental Demand' question='13' onClick={onComparisonSelect} />
    //             <ComparisonButton label1='Physical Demand' label2='Performance' question='14' onClick={onComparisonSelect} />
    //             <ComparisonButton label1='Temporal Demand' label2='Effort' question='15' onClick={onComparisonSelect} />
    //         </div>
    //     </div>
    // );

    return (
        <div>
            <h2>Iteration {stateIteration} Survey Task</h2>
            <div>
                <h4>
                    You have just identified differences between a couple of generative design alternatives from the given solution space.
                    The following survey will ask you about your experience during the task, please select the responses that most closely relate to your user experience.
                </h4>

                <div style={{ marginBottom: '10px' }}>
                    <p>
                        {radioQuestions}
                    </p>
                </div>

                <div>
                    {questionaire}
                </div>

                <div style={{ marginTop: '15px' }}>
                    <h5>Iteration {stateIteration} Complete!</h5>
                    {/* Thank you for completing Iteration {stateIteration} of the Generative Design Study!
                    The following page will take you to Iteration {stateIteration + 1}, where you
                    will complete similar tasks with a different design space. */}
                </div>
            </div>

            {/* figure out disabled */}
            <button className='button' onClick={() => next()} >
                Next
            </button>
        </div>
    );
};

export default VisualDissimilaritySurvey;
