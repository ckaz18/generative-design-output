import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import store from '../../store/configureStore';


const SignIn = () => {
  const token = useSelector((state) => state.signIn.token);
  const userId = useSelector((state) => state.signIn.userId);
  const history = useHistory();
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();

  const [responseUserId, setResponseUserId] = useState(null);
  const [responseToken, setResponseToken] = useState(null);
  const [stateToken, setStateToken] = useState();
  const [hasSignedIn, setHasSignedIn] = useState(false);

  console.log(`!!token && !!userId ${!!token && !!userId}`);
  if (!!token && !!userId) {
    history.push('/');
  }


  useEffect(() => {
    if (hasSignedIn && responseUserId && responseToken) {
      console.log("useEffect hasSignedIn")
      store.dispatch({ type: 'LOGIN_SUCCESS', token: responseToken, userId: responseUserId });
    }
  }, [hasSignedIn]);


async function signin(credentials,) {
  try {
    const signinResponse = await fetch('/signin', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json', Accept: 'application/json' },
      body: JSON.stringify(credentials),
        });
        const signinMetadata = await signinResponse.json();
        if (signinMetadata.error) {
            alert(signinMetadata.error);
        } else {
          console.log(`signin metadata: ${signinMetadata.token}, store ${token} ${userId}`)
          setResponseUserId(signinMetadata.userId)
          setResponseToken(signinMetadata.token)
          setHasSignedIn(true);
        }
    } catch (error) {
      console.log({error, stack: JSON.stringify(error.stack)});
      alert(error.message);

    }
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    await signin({
      user: `${firstName} ${lastName}`,
      userToken: stateToken,
    }, );
  };

  // Add validation
  return (
    <div className='login-wrapper'>
      <h1>First, Please Login</h1>
      <form onSubmit={handleSubmit}>
        <label>
          <p style={{ paddingTop: '1rem' }}>First Name</p>
          <input type='text' onChange={(e) => setFirstName(e.target.value)} />
        </label>

        <label>
          <p style={{ paddingTop: '1rem' }}>Last Name</p>
          <input type='text' onChange={(e) => setLastName(e.target.value)} />
        </label>

        <label>
          <p style={{ paddingTop: '1rem' }}>Token</p>
          <input type='text' onChange={(e) => setStateToken(e.target.value)} />
        </label>
        <div style={{ paddingTop: '1rem', position: 'absolute', left: '46%' }}>
          <button style={{height: '2.5rem', width: '5.5rem'}} type='submit'>Submit</button>
        </div>
      </form>
    </div>
  );
};

SignIn.propTypes = {
  // setToken: PropTypes.func.isRequired,
};

export default SignIn;
