import React from 'react';
import { useLocation } from 'react-router';

const ErrorPage = () => {
    const location = useLocation();
    const error = location.state.error;
    const message = location.state.message;

    let errorMessage = 'An error has occurred. Please contact Michael Botyarov at mbotyaro@colostate.edu. \n\n';

    console.log({ error, message });
    return (
        <div style={{ margin: '1rem' }}>
            {errorMessage}
            <div style={{ fontFamily: 'Courier New' }}> <br />{message ? message : null}<br />{error ? error.message : null}</div>
        </div >
    );
};

export default ErrorPage;