import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Comparison from './iteration/Comparison';
import ErrorPage from './common/ErrorPage';
import Header from './common/Header';
import HomePage from './home/HomePage';
import Iteration from './iteration/Iteration';
import Transition from './iteration/Transition';
import PageNotFound from './PageNotFound';
import PairwiseComparison from './userSurvey/PairwiseComparison';
import SignIn from './signin/SignIn';
import UserSurvey from './userSurvey/UserSurvey';
import { useSelector } from 'react-redux';
import VisualDissimilaritySurvey from './userSurvey/VisualDissimilaritySurvey';


function App() {
  const token = useSelector((state) => state.signIn.token);
  const userId = useSelector((state) => state.signIn.userId);
  console.log({token, userId});

  return (
    <div className='container-fluid'>
      <div className='nav'>
        <BrowserRouter>
          <Header />
          <Switch>
            <Route path={'/signin'} component={SignIn}/>
            <Route exact path='/' exact component={HomePage} />
            <Route path={'/iteration'} component={Iteration} />
            <Route path='/user-survey' component={UserSurvey} />
            <Route path='/comparison' component={Comparison} />
            <Route path='/visual-dissimilarity-survey' component={VisualDissimilaritySurvey} />
            <Route path='/pairwise-comparison-survey' component={PairwiseComparison} />
            <Route path='/transition' component={Transition} />
            <Route path='/error-page' component={ErrorPage} />
            <Route component={PageNotFound} />
          </Switch>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
