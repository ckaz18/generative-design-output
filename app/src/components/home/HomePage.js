import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import  { useSelector, useDispatch } from 'react-redux';


const HomePage = () => {
  const history = useHistory();
  const token = useSelector((state) => state.signIn.token);
  const userId = useSelector((state) => state.signIn.userId );

  if (!token && !userId) {
    history.push('/signin');
  }
  
  // const lastIteration = useSelector((state) => state.lastIteration.iteration);
  const stateIteration = useSelector((state) => state.userIteration.iteration);
  const [iteration, setIteration] = useState(null);
  const dispatch = useDispatch();
  
  console.log({ token, userId, stateIteration, iteration });


  useEffect(() => {
    async function getIteration() {
      try {
        
        const res = await fetch(`/latest_iteration?token=${token}&userId=${userId}`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
        });

        const data = await res.json();

        if (!!data.iteration) {
          setIteration(data.iteration);
          dispatch({type: 'CURRENT_ITERATION', iteration: data.iteration });
        }

      } catch (error) {
        console.log({ error });
        alert(`Something went wrong: ${error}`);
      }
    }

    if (iteration === null && userId && token) {
      getIteration();
    }

  }, [iteration, token, userId]);


  const beginStuff = () => {
    history.push('/iteration');
  };

  return (
    <div className='primary'>
      <h1>Welcome to the Generative Design Study!</h1>
      <div>
        Thank you for participating in this study! Below is an introduction that
        presents the background context of the study. Specific instructions will
        be provided on subsequent pages. Your involvement in the study is
        completely voluntary and you are free to withdraw at any time.
      </div>
      <div>
      The subsequent iterative study sections will guide you through two tasks related to parsing 
        aircraft engine loading bracket design produced via the Autodesk Fusion 360 generative design process. 
        During the first task, there will be a box that periodically appears with 'Click Me' on it. 
        Please click the box whenever you notice it appear on the screen. 
        Note that there will be seven iterations total to complete your participation in the study and the system will inform you when the study is complete as you will be redirected to the login screen.
      </div>
      <div>
        <h4>Overview of Generative Design</h4>
        Generative design operates at the conceptual phase of design, where the
        design is still under formulation. Generative design exists to
        facilitate the design process by providing novel solutions to complex
        problems that designers may have otherwise been inefficient in solving
        or even unable to solve. Generative design tools have been developed to
        ingest the problem definition as input to produce feasible solutions for
        the given problem. Due to the inherently ambiguous nature of the
        conceptual design phase, generative design inputs can be directly
        derived from system requirements (e.g. manufacturing process, weight,
        material type, etc.). In cases where the problem is abstract or novel,
        such that the designer does not have a clear starting point, leveraging
        generative design will enable the designer to explore the extent of the
        design envelope by analyzing a greater quantity of design possibilities
        when compared to traditional modeling processes.
      </div>
      <div>
        <h4>Study Task Background</h4>
        Modern aircraft engines utilize loading brackets to support the weight
        of the engine during operations without breaking or warping. Although
        they may only be used periodically, loading brackets remain installed on
        the engine, even during flight. Creating a new loading bracket via
        generative design processes would improve engine efficiency since
        generative design tools leverage unique geometry to optimize component
        weight and other parameters (e.g. volume), given any identified
        requirements. As is the case with various aerospace components, it is
        crucial to develop a design that has minimal weight without trading off
        much strength and performance. The figure below illustrates what a
        common loading bracket looks like.
      </div>
      <div style={{ margin: '16px 0px' }}>
        <img
          src='/images/figure_1.png'
          style={{ height: '300px', display: 'block', margin: 'auto' }}
          alt='Sample Aircraft Engine Loading Bracket'
        />
      </div>
      <div>
        In order to simulate the operational environment, Autodesk Fusion 360
        was used as the generative design tool to produce design alternatives
        for your review. The following base requirements were input into
        Autodesk Fusion 360 prior to the generation of design alternatives.
        <div>
          <ul style={{ listStyleType: 'circle' }}>
            <li>Maximum static linear load of 8,000 pounds vertical</li>
            <li>Maximum static linear load of 8,500 pounds horizontal.</li>
            <li>
              Maximum static linear load of 9,500 pounds 42 degrees from
              vertical.
            </li>
            <li>
              Maximum static torsional load of 5,000 lb-in horizontal at
              intersection of centerline of pin and midpoint between clevis
              arms.
            </li>
            <li>
              Any machine bolt interface (0.375-24 AS3239-26) is to be composed
              of nut face 0.405 inches maximum inside diameter and 0.558 inches
              minimum outside diameter. For the study, the bolts are to be
              considered infinitely stiff.
            </li>
            <li>
              The pin interface is to be 0.75 inches in diameter pin. For the
              study, the pin is to be considered infinitely stiff.
            </li>
          </ul>
        </div>
       
      </div>
      {/* <div style={{ margin: '16px 0px' }}>
        <img
          src='/images/figure_2.png'
          style={{ height: '300px', display: 'block', margin: 'auto' }}
          alt='Overall Study Structure'
        />
      </div> */}
      {/* add timer on click */}
      <button className='button' onClick={() => beginStuff()}>
        Begin
      </button>
    </div>
  );
};

export default HomePage;
