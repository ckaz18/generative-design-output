import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

const Comparison = () => {
  const stateIteration = useSelector((state) => state.userIteration.iteration);
  const token = useSelector((state) => state.signIn.token);
  const userId = useSelector((state) => state.signIn.userId);
  const history = useHistory();
  // const [startTime] = useState(setCurrentTime());
  const [images, setImages] = useState([]);

  const [selectedCss, setSelectedCss] = useState('');
  const [selectedIndex, setSelectedIndex] = useState(null);

  const [isLoading, setIsLoading] = useState(true);
  const [startTimer, setStartTimer] = useState(null);

  const setCurrentTime = () => {
    return new Date().getTime();
  };

  useEffect(() => {
    if (!isLoading && !startTimer) {
      setStartTimer(setCurrentTime());
    }

    async function getComparisonImages() {
      try {
        const res = await fetch(`/comparison_images?iteration=${stateIteration}&token=${token}`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
          redirect: 'follow',
        });
        const data = await res.json();
        setImages(data.images);
        setIsLoading(false);
      } catch (error) {
        history.push('/error-page', { error, message: 'Error retrieving images for comparison.' });
      }
    }
    if (images.length === 0) {
      getComparisonImages();
    }
    if (selectedIndex !== null) {
      goToNext();
    }
  }, [selectedIndex, stateIteration, isLoading]);


  const goToNext = () => {
    const confirmBox = window.confirm(
      'Are you ready to submit your design comparison selection?'
    );
    if (confirmBox) {
      goToSurvey();
    } else {
      setSelectedIndex(null);
    }
  };

  const onSelectClick = (element) => {
    if (selectedIndex) {
      setSelectedIndex(null);
      setSelectedCss('');
    } else {
      setSelectedCss('#8f8f8f');
      setSelectedIndex(parseInt(element.target.id));
    }
  };

  const goToSurvey = () => {
    const now = setCurrentTime();
    const timeElapsed = now - startTimer;
    // make call with seconds and iteration to API/DB

    fetch('/comparison', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        iterationId: stateIteration,
        taskNumber: 'image-comparison',
        time: timeElapsed / 1000,
        token,
        userId,
        userInput: selectedIndex,
      }),
    }).then(() => {
      history.push('/visual-dissimilarity-survey');
      // history.push('/user-survey');
    }).catch((error) => {
      history.push('/error-page', { error, message: 'Error attempting to store user comparison selection.' });
    });
    // history.push('/user-survey');
  };

  const sideInstructionsJSX = (
    <div className='primary' style={{ position: 'sticky', top: '100px' }}>
      <h4>Iteration {stateIteration} Visual Dissimilarity Task</h4>
      Click the one design alternative that you find to be most visually different.
    </div>
  );

  const imagesJSX = (
    <div
      style={{
        justifyContent: 'center',
        margin: '0 auto',
        width: '70%',
      }}
    >
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: 'repeat(2, 1fr)',
          justifyContent: 'space-evenly',
          margin: '10px',
        }}
      >
        {images.length > 0
          ? images.map((image, idx) => {
            return (
              <div
                key={idx}
                style={{
                  backgroundColor: selectedIndex === idx ? selectedCss : null,
                  border: 'solid gray 1px',
                  flex: '2 1 auto',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                id={idx}
                onClick={onSelectClick}
              >
                <img
                  style={{
                    margin: 'auto 10px',
                  }}
                  src={`data:image/jpeg;base64,${image}`}
                  alt={`Comparison GDO ${idx + 1}`}
                  id={idx}
                  onClick={onSelectClick}
                ></img>
              </div>
            );
          })
          : null}
      </div>
    </div>
  );

  // May not need sticky
  return (
    <div style={{ padding: '8px', display: 'flex', width: '97%' }}>
      <div className='left-box'>{imagesJSX}</div>
      <div className='right-box'>{sideInstructionsJSX}</div>
    </div>
  );
};

export default Comparison;
