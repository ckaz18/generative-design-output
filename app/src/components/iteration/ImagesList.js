
const imageList = {
  1: 1000,
  2: 500,
  3: 250,
  4: 100,
  5: 50,
  6: 25,
  7: 10
}

const getImageFromIteration = (iteration) => {
  if (typeof iteration === 'string') {
    return imageList[parseInt(iteration)];
  } else {
    return imageList[iteration]
  }
};

export default getImageFromIteration;