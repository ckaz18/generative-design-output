/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { getIterationText } from './IterationText';
import { createTracerId } from '../../helpers/helpers';


const distractionArray = [];

const Iteration = () => {
  const token = useSelector((state) => state.signIn.token);
  const userId = useSelector((state) => state.signIn.userId );
  const stateIteration = useSelector((state) => state.userIteration.iteration);

  const [images, setImages] = useState([]);
  const [imageInfo, setImageInfo] = useState([]);
  const [selectedCss, setSelectedCss] = useState('');
  const [selectedIndex, setSelectedIndex] = useState(-1);
  const [isLoading, setIsLoading] = useState(true);
  const [startTime, setStartTime] = useState(null);
  const [distractionStartTime, setDistractionStartTime] = useState(null);
  const [distractionSumbission, setDistractionSumbission] = useState(false);
  const [distractionBoxVisible, setDistractionBoxVisible] = useState(false);
  const [waitingTimer, setWaitingTimer] = useState(null);


  const setCurrentTime = () => {
    return new Date().getTime();
  };

  const getRandomNumber = () => {
    return Math.floor(Math.random() * (20 - 5 + 1) + 5) * 1000;
  }

  const clearTimer = (timerId) => {
    clearTimeout(timerId);
  }

  const recordDistraction = () => {
    const currentMoment = setCurrentTime();
    if (waitingTimer) {
      distractionArray.push({
        currentDistractionTime: currentMoment - distractionStartTime,
        distractionTimeFromStart: currentMoment - startTime,
        timer: waitingTimer
      });

      clearTimer(waitingTimer);

      setDistractionBoxVisible(false);
      setDistractionStartTime(null);
      setWaitingTimer(null);
    }
  };

  const startDistractionSet = () => {
    if (!waitingTimer && !distractionSumbission) {
      const timerMs = getRandomNumber();

      const timer = setTimeout(() => {
        setDistractionBoxVisible(true);
        setDistractionStartTime(setCurrentTime());
      }, timerMs);

      setWaitingTimer(timer);
    }
  };

  useEffect(() => {
    if (!isLoading && !startTime) {
      setStartTime(setCurrentTime());
    }

    async function getIterationImages() {
      try {
        const res = await fetch(`/iteration_images?iteration=${stateIteration}&token=${token}`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
          redirect: 'follow',
        });

        const data = await res.json();
        setImages(data.images);
        setImageInfo(data.imageData);
        setIsLoading(false);

      } catch (error) {
        history.push({ pathname: '/error-page', state: { error, message: 'Error retrieving images for iteration.' } });
      }
    };

    // TODO: calling startDistractionSet twice..
    if (!images.length) {
      getIterationImages();
      startDistractionSet();
    }

    if (!!images.length && !distractionSumbission && !waitingTimer) {
      startDistractionSet();
    }

    if (selectedIndex > -1) {
      goToNext();
    }
    
  }, [selectedIndex, waitingTimer, stateIteration, isLoading]);

  const history = useHistory();

  const goToNext = () => {
    console.log({ selectedIndex });
    let confirmBox;
      confirmBox = window.confirm(
        'Are you ready to submit your design selection?'
      );
      if (confirmBox) {
        setSelectedIndex(-1);
        submit();
      } else {
        setSelectedIndex(-1);
      }
  };
  const submit = () => {
    setDistractionSumbission(true);

    const now = new Date().getTime();
    const timeElapsed = now - startTime;
    // make call with seconds and iteration to API/DB
    // record distraction
    recordDistraction();

    const postTraceId = createTracerId();
    console.log({ postTraceId });

    fetch('/iteration', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        distractionArray,
        iterationId: stateIteration,
        time: timeElapsed / 1000,
        token,
        traceId: postTraceId,
        userId,
        userInput: {
          imageInfo: imageInfo[selectedIndex],
          idx: selectedIndex
        },
      }),
    }).then(() => history.push('/user-survey')).catch((error) => {

      history.push('/error-page', { error, message: 'Error attempting to store user selection for iteration.' });
    });

  };

  // TODO: verify for first, middle, last, reselect
  const onSelectClick = (element) => {
    const selected = parseInt(element.target.id);
    if (selected === selectedIndex) { //|| selectedIndex != 0
      setSelectedIndex(-1);
      setSelectedCss('');
    } else {
      setSelectedCss('#8f8f8f');
      setSelectedIndex(parseInt(element.target.id));
    }
  };

  const iterationTextValues = getIterationText(stateIteration);

  const formattedImages = (
    images.length > 0
      ? images.map((image, idx) => {
        // eslint-disable-next-line jsx-a11y/img-redundant-alt
        return (
          <div
            className='iteration-image-container'
            key={idx}
            style={{
              backgroundColor: selectedIndex === idx ? selectedCss : null,
            }}
            id={idx}
            onClick={onSelectClick}
          >
            <img
              style={{ height: '45%' }}
              className='iteration-image'
              src={`data:image/jpeg;base64,${image}`}
              alt={`Generative Design Image ${idx + 1}`}
              id={idx}
              onClick={onSelectClick}
            />
            <div
              style={{
                height: '13rem',
                margin: 3,
                whiteSpace: 'pre-wrap',
                // content: '/a'
              }}
            >
              {imageInfo.length > 0 ? (
                <p>
                  Manufacturing Method: {imageInfo[idx]["ManufacturingMethod"]}<br/>
                  Mass: {imageInfo[idx]["Mass"]}<br/>
                  Material: {imageInfo[idx]["Material"]}<br/>
                  Max VonMises Stress: {imageInfo[idx]["MaxVonMisesStress"]}<br/>
                  Volume: {imageInfo[idx]["Volume"]}
                </p>
              ) : null}
              {/* {imageInfo.length > 0
                ? imageInfo.map((k, innerIdx, innerArr) => {
                    // if (k === 'cluster' || k === 'id' || k === 'OutcomeID') {
                    //   // eslint-disable-next-line array-callback-return
                    //   return;
                    // } else {
                      // const comma =
                      //   innerIdx === innerArr.length - 2 ? ' ' : ', ';
                      // let key = k.match(/[A-Z][a-z]+/g);
                      // if (key !== null) {
                      //   key = key.join(' ');
                      // } else {
                      //   key = k;
                      // } 
                      // {"ManufacturingMethod":"Additive","Mass":"5.343","Material":"Inconel 718","MaxVonMisesStress":"37322.7","Volume":"18.29"},
                      return (
                        <div>
                          <p>Manufacturing Method: {k["ManufacturingMethod"]}</p>
                          <p>Mass: {k["Mass"]}</p>
                          <p>Material: {k["Material"]}</p>
                          <p>Max VonMises Stress: {k["MaxVonMisesStress"]}</p>
                          <p>Volume: {k["Volume"]}</p>
                        </div>
                      );
                      // return record;
                    // }
                  })
                : null} */}
            </div>
          </div>
        );
      })
      : null
  );


  const theRedButton = (
    <button style={{ width: '3rem', height: '3rem', backgroundColor: '#FFC1C3', marginLeft: '2.5rem', marginTop: '10rem' }} onClick={() => recordDistraction()}>Click Me</button>
  );

  const topInstructions = (
    <>
      <h4>Iteration {stateIteration} Design Selection Task</h4>
      <br />
      Click a single design alternative given the following requirements:
      <div>
        {/* try in ol as well */}
        <ol style={{ listStyleType: 'lower-alpha' }}>
          <li style={{ marginBottom: '3px' }}>
            Mass must be between {iterationTextValues['startMass']} and{' '}
            {iterationTextValues['endMass']} lbs.
          </li>
          <li style={{ marginBottom: '3px' }}>
            {iterationTextValues['manufacturingMethod']} method must be used.
          </li>
          <li style={{ marginBottom: '3px' }}>
            {iterationTextValues['material']} material must be used.
          </li>
          <li style={{ marginBottom: '3px' }}>
            Max von Mises Stress much be between{' '}
            {iterationTextValues['startMaxVonMisesStress']} and{' '}
            {iterationTextValues['endMaxVonMisesStress']} psi.
          </li>
          <li style={{ marginBottom: '3px' }}>
            Volume must be between {iterationTextValues['startVolume']} and{' '}
            {iterationTextValues['endVolume']} cubic inches.
          </li>
          <li>
            Part must have at least {iterationTextValues['anchorPoints']} vertical drill holes.
          </li>
        </ol>
      </div>
    </>
  );

  const imagesJSX = (
    <div
      className='iteration-image-section'
    >
      {isLoading ? <div style={{ textAlign: 'center', fontSize: 'large' }}>Loading Images...</div> : formattedImages}
    </div>
  );

  return (
    <div style={{ padding: '8px' }}>
      <div style={{ width: '99%', display: 'flex' }}>
        <div className='left-box'>{imagesJSX}</div>

        <div className='right-box'>
          <div style={{ position: 'sticky', top: 0 }}>
            <div>{topInstructions}</div>
            {distractionBoxVisible ? theRedButton : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Iteration;
