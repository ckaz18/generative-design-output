import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import store  from '../../store/configureStore';

const Transition = () => {
  // Need to use useEffect potentially
  const stateIteration = useSelector((state) => state.userIteration.iteration);
  const history = useHistory();
  const lastIteration = useSelector((state) => state.lastIteration.iteration);
  
  const nextIteration = stateIteration + 1;
  const lastCurrentIteration = stateIteration;

  // if (lastIteration >= stateIteration) {
  //   // not updating state on 6 correctly
  //   store.dispatch({ type: 'CURRENT_ITERATION', iteration: nextIteration });
  // }

  useEffect(() => {
  console.log({beforeUseEffect: true, stateIteration, lastIteration, nextIteration, lastCurrentIteration});
    if (lastIteration === stateIteration) {
      // not updating state on 6 correctly
      store.dispatch({ type: 'CURRENT_ITERATION', iteration: nextIteration });
    }
    // else if (lastIteration <= stateIteration) {
    //   store.dispatch({ type: 'CURRENT_ITERATION', iteration: stateIteration++ });
    // }
  }, [ lastIteration ]);

  console.log({ afterUseEffect: true, stateIteration, lastIteration, nextIteration, lastCurrentIteration});


  let nextPageText = '';
  if (lastIteration === 7) {
    nextPageText = 'Thank you for participating!';
  }
  else {
    nextPageText = 'Click "Next" to continue to the next iteration.';
  }

  const next = () => {
    history.push('/iteration');
  }

  const nextButton = (
    <button className='button' onClick={() => next()} >
      Next
    </button>
  );
  
  const isNotLastIteration = lastIteration !== 7;
  const finalDisplay = isNotLastIteration ? <div style={{padding: '2px', paddingTop: '5px'}}>{nextButton}</div> : '';  
  
  console.log("sessionStorage items:", sessionStorage.getItem("token"), sessionStorage.getItem("userId"));

  return (
    <div>
      You just completed iteration {lastIteration}!
      <br/>
      {nextPageText}

      {finalDisplay}
    </div>
  );
};

export default Transition;