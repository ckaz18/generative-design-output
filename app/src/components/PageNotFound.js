import React from 'react';

const PageNotFound = () => <h1>Uh Oh! Page not found.</h1>

export default PageNotFound;