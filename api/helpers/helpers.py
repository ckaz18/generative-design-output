
# Might just need to return current
# If user_id is tied to iteration (is the iteration), then get current
def calculate_latest_iteration(iteration_list, is_next=False):
  # is_next is for the next iteration when logging back into the app
  if iteration_list is None or len(iteration_list) == 0:
    return 1
  else: 
    last_iteration = iteration_list[len(iteration_list) - 1].iteration_sequence
    if last_iteration < 7:
      if is_next:
        return last_iteration + 1
      else:
        return last_iteration  
    else:
      return -1
