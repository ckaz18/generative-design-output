import sys

comparison_image_id = {
  "1": ["Outcome 105 (S13O6)", "Outcome 106 (S13O7)", "Outcome 145 (S17O7)", "Outcome 107 (S13O8)"],
  "2": ["Outcome 777 (S36O50)", "Outcome 550 (S32O35)", "Outcome 751 (S36O20)", "Outcome 577 (S32O80)"],
  "3": ["Outcome 620 (S33O40)", "Outcome 606 (S33O25)", "Outcome 795 (S36O74)", "Outcome 758 (S36O29)"],
  "4": ["Outcome 325 (S25O24)", "Outcome 221 (S20O16)", "Outcome 908 (S38O32)", "Outcome 880 (S37O85)"],
  "5": ["Outcome 462 (S30O36)", "Outcome 765 (S36O36)", "Outcome 384 (S28O11)", "Outcome 956 (S38O83)"],
  "6": ["Outcome 269 (S23O10)", "Outcome 831 (S37O16)", "Outcome 211 (S20O5)", "Outcome 382 (S28O9)"],
  "7": ["Outcome 846 (S37O33)", "Outcome 415 (S29O13)", "Outcome 211 (S20O5)", "Outcome 956 (S38O83)"]
}

# filters
'''
"Material", "1"
"ManufacturingMethod", "2"
"Mass", "3"
"MaxVonMisesStress", "4"
"Volume", "5"
"cluster", "6"
"id", "7" -  no filter
'''
comparison_image_id_with_filter = {
  "1": [["Outcome 1 (S1O5)", "Outcome 2 (S1O6)", "Outcome 3 (S1O7)", "Outcome 37 (S7O25)"],["Outcome 34 (S7O22)", "Outcome 38 (S7O27)", "Outcome 57 (S9O20)", "Outcome 60 (S9O24)"],["Outcome 41 (S7O38)", "Outcome 42 (S7O39)", "Outcome 45 (S7O42)", "Outcome 47 (S7O44)"],["Outcome 136 (S16O21)", "Outcome 155 (S17O22)", "Outcome 196 (S19O18)", "Outcome 240 (S21O22)"],["Outcome 382 (S28O9)", "Outcome 384 (S28O11)", "Outcome 415 (S29O13)", "Outcome 436 (S29O43)"],["Outcome 155 (S17O22)", "Outcome 351 (S27O8)", "Outcome 384 (S28O11)", "Outcome 846 (S37O33)"],["Outcome 269 (S23O10)", "Outcome 346 (S26O25)", "Outcome 483 (S31O11)", "Outcome 846 (S37O33)"]],
  "2": [["Outcome 2 (S1O6)", "Outcome 3 (S1O7)", "Outcome 4 (S1O8)", "Outcome 26 (S7O13)"],["Outcome 32 (S7O20)", "Outcome 38 (S7O27)", "Outcome 44 (S7O41)", "Outcome 45 (S7O42)"],["Outcome 41 (S7O38)", "Outcome 42 (S7O39)", "Outcome 45 (S7O42)", "Outcome 47 (S7O44)"],["Outcome 44 (S7O41)", "Outcome 272 (S23O13)", "Outcome 274 (S23O16)", "Outcome 415 (S29O13)"],["Outcome 155 (S17O22)", "Outcome 269 (S23O10)", "Outcome 382 (S28O9)", "Outcome 572 (S32O60)"],["Outcome 196 (S19O18)", "Outcome 382 (S28O9)", "Outcome 727 (S35O93)", "Outcome 880 (S37O85)"],["Outcome 43 (S7O40)", "Outcome 269 (S23O10)", "Outcome 346 (S26O25)", "Outcome 846 (S37O33)"]],
  "3": [["Outcome 1 (S1O5)", "Outcome 2 (S1O6)", "Outcome 3 (S1O7)", "Outcome 37 (S7O25)"],["Outcome 32 (S7O20)", "Outcome 34 (S7O22)", "Outcome 38 (S7O27)", "Outcome 39 (S7O36)"],["Outcome 41 (S7O38)", "Outcome 42 (S7O39)", "Outcome 44 (S7O41)", "Outcome 45 (S7O42)"],["Outcome 152 (S17O14)", "Outcome 155 (S17O22)", "Outcome 156 (S17O23)", "Outcome 272 (S23O13)"],["Outcome 85 (S12O9)", "Outcome 93 (S12O17)", "Outcome 152 (S17O14)", "Outcome 346 (S26O25)"],["Outcome 43 (S7O40)", "Outcome 152 (S17O14)", "Outcome 238 (S21O16)", "Outcome 269 (S23O10)"],["Outcome 211 (S20O5)", "Outcome 269 (S23O10)", "Outcome 415 (S29O13)", "Outcome 956 (S38O83)"]],
  "4": [["Outcome 1 (S1O5)", "Outcome 2 (S1O6)", "Outcome 3 (S1O7)", "Outcome 37 (S7O25)"],["Outcome 32 (S7O20)", "Outcome 34 (S7O22)", "Outcome 38 (S7O27)", "Outcome 39 (S7O36)"],["Outcome 42 (S7O39)", "Outcome 44 (S7O41)", "Outcome 45 (S7O42)", "Outcome 47 (S7O44)"],["Outcome 152 (S17O14)", "Outcome 155 (S17O22)", "Outcome 156 (S17O23)", "Outcome 209 (S20O3)"],["Outcome 85 (S12O9)", "Outcome 93 (S12O17)", "Outcome 152 (S17O14)", "Outcome 346 (S26O25)"],["Outcome 346 (S26O25)", "Outcome 351 (S27O8)", "Outcome 366 (S27O29)", "Outcome 382 (S28O9)"],["Outcome 296 (S23O48)", "Outcome 415 (S29O13)", "Outcome 831 (S37O16)", "Outcome 956 (S38O83)"]],
  "5": [["Outcome 1 (S1O5)", "Outcome 2 (S1O6)", "Outcome 3 (S1O7)", "Outcome 37 (S7O25)"],["Outcome 32 (S7O20)", "Outcome 34 (S7O22)", "Outcome 38 (S7O27)", "Outcome 39 (S7O36)"],["Outcome 41 (S7O38)", "Outcome 42 (S7O39)", "Outcome 44 (S7O41)", "Outcome 45 (S7O42)"],["Outcome 152 (S17O14)", "Outcome 155 (S17O22)", "Outcome 156 (S17O23)", "Outcome 272 (S23O13)"],["Outcome 85 (S12O9)", "Outcome 152 (S17O14)", "Outcome 196 (S19O18)", "Outcome 540 (S32O24)"],["Outcome 238 (S21O16)", "Outcome 269 (S23O10)", "Outcome 346 (S26O25)", "Outcome 632 (S33O55)"],["Outcome 483 (S31O11)", "Outcome 831 (S37O16)", "Outcome 846 (S37O33)", "Outcome 956 (S38O83)"]],
  "6": [["Outcome 992 (S40O21)", "Outcome 527 (S32O9)", "Outcome 984 (S40O9)", "Outcome 399 (S28O35)"],["Outcome 995 (S40O25)", "Outcome 540 (S32O24)", "Outcome 988 (S40O13)", "Outcome 395 (S28O28)"],["Outcome 823 (S37O4)", "Outcome 181 (S19O2)", "Outcome 594 (S33O9)", "Outcome 85 (S12O9)"],["Outcome 614 (S33O34)", "Outcome 984 (S40O9)", "Outcome 366 (S27O29)", "Outcome 415 (S29O13)"],["Outcome 540 (S32O24)", "Outcome 325 (S25O24)", "Outcome 483 (S31O11)", "Outcome 894 (S37O104)"],["Outcome 211 (S20O5)", "Outcome 238 (S21O16)", "Outcome 152 (S17O14)", "Outcome 572 (S32O60)"],["Outcome 831 (S37O16)", "Outcome 483 (S31O11)", "Outcome 846 (S37O33)", "Outcome 956 (S38O83)"]],
  "7": [["Outcome 343 (S26O18)", "Outcome 935 (S38O61)", "Outcome 232 (S21O10)", "Outcome 867 (S37O70)"],["Outcome 738 (S36O3)", "Outcome 934 (S38O60)", "Outcome 635 (S33O59)", "Outcome 497 (S31O25)"],["Outcome 249 (S22O24)", "Outcome 878 (S37O83)", "Outcome 382 (S28O9)", "Outcome 177 (S18O26)"],["Outcome 884 (S37O92)", "Outcome 156 (S17O23)", "Outcome 668 (S35O6)", "Outcome 360 (S27O21)"],["Outcome 221 (S20O16)", "Outcome 463 (S30O37)", "Outcome 85 (S12O9)", "Outcome 632 (S33O55)"],["Outcome 440 (S29O48)", "Outcome 43 (S7O40)", "Outcome 546 (S32O31)", "Outcome 831 (S37O16)"],["Outcome 846 (S37O33)", "Outcome 43 (S7O40)", "Outcome 211 (S20O5)", "Outcome 269 (S23O10)"]]
}


def sort_images_by_iteration(token_filter, images):
  sorted_images = []
  
  print(f"sort_images_by_iteration token: {token_filter}")
  sys.stdout.flush()

  if token_filter == 1: # Material
    sorted_images = sorted(images, key=lambda i: i['Material'])
  elif token_filter == 2: # manufacturingMethod
    sorted_images = sorted(images, key=lambda i: i['ManufacturingMethod'])
  elif token_filter == 3: # mass
    sorted_images = sorted(images, key=lambda i: i['Mass'])
  elif token_filter == 4: # stress
    sorted_images = sorted(images, key=lambda i: i['MaxVonMisesStress'])
  elif token_filter == 5: # volume
    sorted_images = sorted(images, key=lambda i: i['Volume'])
  elif token_filter == 6: # cluster
    sorted_images = sorted(images, key=lambda i: i['cluster'])
  else:
    sorted_images = images
  
  return sorted_images


def get_filtered_list(filter, iteration):

  if filter < 8:
    return comparison_image_id_with_filter.get(f"{filter}")[iteration - 1]
  else:
    return comparison_image_id_with_filter.get("7")[iteration - 1]
