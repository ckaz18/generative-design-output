
## SETUP

1. install python 3.8
1. install virtualenv
  
  - `pip3 install virtualenv`

1. `virtualenv gdo-env`
1. `source gdo-env/bin/activate`
1. When not in use, `deactivate`


### After initial install

1. `pip install -r requirements.txt`

## Deployment
1. setup heroku remote
  `heroku git:remote -a generative-design-study`

## Running
1. `flask run` will start the api under http://localhost:5000/
1. Using Heroku: `heroku local web --port 5001`

## Migrations

# Create a migration
1. `alembic revision --autogenerate -m "<informative description>"`

# Run the migration
1. Update script with upgrade & downgrade
1. `alembic upgrade +1`