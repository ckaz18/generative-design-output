"""Update iterations to different primary key

Revision ID: 88401f0f3f6b
Revises: 
Create Date: 2022-03-19 13:59:33.464741

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '88401f0f3f6b'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # Drop CONSTRAINT
    op.execute('ALTER TABLE iterations DROP CONSTRAINT pk_iterations CASCADE')
    op.execute('ALTER TABLE iterations DROP CONSTRAINT iterations_user_id_fkey')

    # Add COLUMN and Update primary key
    op.add_column('iterations', sa.Column('id', sa.Integer(), primary_key=True))
    op.create_foreign_key("iterations_user_id_fkey", "iterations", "gdo_users", ["user_id"], ["user_id"])


def downgrade():
    op.create_primary_key("pk_iterations", "iterations", ["iteration_id"])
    op.drop_constraint("iterations_fk", "iterations", type_="foreignkey")
    op.drop_column('iterations', 'id')
