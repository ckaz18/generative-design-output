"""Add distraction table

Revision ID: a41c635d82b0
Revises: 88401f0f3f6b
Create Date: 2022-06-21 18:53:21.050556

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a41c635d82b0'
down_revision = '88401f0f3f6b'
branch_labels = None
depends_on = None


def upgrade():
    op.create_unique_constraint('iteration_id_ukey', 'iterations', ['id'])

    op.create_table(
        'distractions',
        sa.Column('id', sa.Integer(), primary_key=True, nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('id'),
        sa.Column('timer', sa.Integer()),
        sa.Column('distraction_from_start_time', sa.BigInteger()),
        sa.Column('distraction_from_current_timer', sa.BigInteger()),
        sa.Column('iteration_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['iteration_id'], ['iterations.id'], name="iterations_id_fkey", ondelete="CASCADE"),
    )

def downgrade():
    op.drop_constraint("iterations_id_fkey", "distractions", type_="foreignkey")
    op.drop_constraint('iteration_id_ukey', 'iterations', type_='unique')
    op.drop_table('distractions')
