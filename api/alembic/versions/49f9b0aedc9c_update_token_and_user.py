"""update_token_and_user

Revision ID: 49f9b0aedc9c
Revises: a41c635d82b0
Create Date: 2022-11-18 13:27:12.805168

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '49f9b0aedc9c'
down_revision = 'a41c635d82b0'
branch_labels = None
depends_on = None


def upgrade():
    # drop the foreign and primary keys
    # op.execute("ALTER TABLE gdo_users DROP CONSTRAINT iterations_user_id_fkey CASCADE")
    op.execute("ALTER TABLE iterations DROP CONSTRAINT iterations_user_id_fkey")
    op.execute("ALTER TABLE survey DROP CONSTRAINT survey_user_id_fkey")
    op.execute("ALTER TABLE gdo_users DROP CONSTRAINT gdo_users_user_token_key CASCADE")
    op.execute("ALTER TABLE gdo_users DROP CONSTRAINT gdo_users_pkey CASCADE")
    # create new column
    op.add_column("gdo_users", sa.Column('id', sa.Integer(), primary_key=True))
    # apply constraints
    op.create_foreign_key("iterations_users_id_fkey", "iterations", "gdo_users", ["user_id"], ["id"])
    op.create_foreign_key("survey_users_id_fkey", "survey", "gdo_users", ["user_id"], ["id"])


def downgrade():
    # drop primary constraint
    op.execute("ALTER TABLE survey DROP CONSTRAINT survey_users_id_fkey")
    op.execute("ALTER TABLE iterations DROP CONSTRAINT iterations_users_id_fkey")
    op.drop_constraint("PRIMARY", "gdo_users", type_="primary")
    # drop column
    op.execute("ALTER TABLE gdo_users DROP COLUMN id")
    # revert to original
    op.create_primary_key("gdo_users_pkey", "gdo_users", ["user_id"])
    op.create_unique_constraint("gdo_users_user_token_key", "gdo_users", ["user_token"])
    op.create_foreign_key(
            "survey_user_id_fkey", "survey",
            "gdo_users", ["user_id"], ["id"])
    op.create_foreign_key(
            "iterations_user_id_fkey", "iterations",
            "gdo_users", ["user_id"], ["id"])

    # op.create_foreign_key("user_id_fkey", "gdo_users", type_="foreignkey")
    # op.create_unique_constraint("survey_user_id_fkey", "survey", type_="foreignkey")
    # op.create_unique_constraint("iterations_user_id_fkey", "iterations", type_="foreignkey")

    
    
