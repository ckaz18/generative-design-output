import os
import sys
from dotenv import dotenv_values
# direnv

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.elements import Null

# VARS & Engine
config = dotenv_values('.env')
database_url = os.environ.get('DATABASE_URL')


if database_url == '':
  raise Error('DATABASE_URL is not available!!')
database_url = database_url.replace("postgres:", "postgresql:")
print(f"DATABASE: {database_url}")
pg_engine = create_engine(database_url, echo=False)
# pg_engine = create_engine(f'postgresql://{config["DB_USERNAME"]}:{config["DB_PASSWORD"]}@{config["DB_HOST"]}:{config["DB_PORT"]}/{config["DB_NAME"]}', echo=False)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=pg_engine)

Base = declarative_base()
# from pg_schemas.images import Images
# from pg_schemas.iteration_images import IterationImages
# from pg_schemas.users import Users
# from pg_schemas.iterations import Iterations

def get_session(engine = pg_engine):
  Session = sessionmaker(bind=engine)
  return Session()
  # return session

def get_session_maker(engine = pg_engine):
  Session = sessionmaker(bind=engine)
  return Session
  # return session
