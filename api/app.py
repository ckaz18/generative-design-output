from dotenv import dotenv_values
import os, sys

from flask import Flask, send_from_directory, _app_ctx_stack
# from flask_restful import Api, Resource, reqparse
from flask_cors import CORS
from api.gdo.route.user import user_bp
from api.gdo.route.iteration import iteration_bp
from api.gdo.route.image import image_bp
from api.gdo.route.survey import survey_bp
from sqlalchemy.orm import scoped_session

from api.pg_schemas.users import Users
from api.pg_schemas.iterations import Iterations
from api.pg_schemas.survey import Survey
from api.pg_schemas.distractions import Distractions
from api.pg_schemas.user_filter import UserFilter 

# from sqlalchemy.orm.scoping import scoped_session #comment this on deployment
from api.db import SessionLocal

app = Flask(__name__, static_folder='../ui', static_url_path='/')
CORS(app) #comment this on deployment
app.session = scoped_session(SessionLocal, scopefunc=_app_ctx_stack.__ident_func__)

app.register_blueprint(user_bp)
app.register_blueprint(iteration_bp)
app.register_blueprint(image_bp)
app.register_blueprint(survey_bp)

# CREATE OR RETRIEVE LOGGER
# logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)

@app.before_request
def create_session():
    pass
    # app.session = get_session(engine)

@app.teardown_appcontext
def shutdown_session(response_or_exc):
    app.session.commit()
    # app.session.remove()
    app.session.close()

@app.route('/')
def index():
    return app.send_static_file('index.html')

@app.route('/<path:path>')
def static_file(path):
    return app.send_static_file(path)

@app.route('/health')
def health_check():
    return {'result': 'Success'}


if __name__ == '__main__':
    # config = dotenv_values('.env')

    # file_handler = FileHandler('gdo_log.log')
    # formatter = logging.Formatter("%(asctime)s | %(pathname)s:%(lineno)d | %(funcName)s | %(levelname)s | %(message)s")
    # file_handler.setFormatter(formatter)

    # logger.addHandler(file_handler)

    app_host = os.environ.get('HOST', "0.0.0.0")
    app_port = os.environ.get('PORT')

    if app_host == '' or app_port == '':
        raise Error('HOST or PORT are not available!!')
    app.run(host=app_host, port=app_port, debug=True)
    sys.stdout.flush()