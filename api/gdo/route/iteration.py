import sys
from flask import Blueprint, request, jsonify
from api.pg_schemas.iterations import Iterations
from api.pg_schemas.distractions import Distractions
from api.pg_schemas.user_filter import UserFilter 
from api.db import pg_engine, get_session
from api.comparison_images.comparison_images_by_iteration import comparison_image_id
from api.helpers.helpers import calculate_latest_iteration


iteration_bp = Blueprint('iteration', __name__, )

@iteration_bp.route('/iteration/', methods=["POST"])
def save_iteration():
  req = request.get_json()
  iteration_sequence = int(req["iterationId"])
  token = int(req["token"])
  user_id = int(req["userId"])
  time = req["time"]
  user_input = req["userInput"]
  distraction_list = req["distractionArray"]

  try:
    trace_id = req.get("traceId", "")
    print(f"TRACE: {trace_id}")
    session = get_session(pg_engine)
    # get record if exists
    original_record = session.query(Iterations).filter_by(
      iteration_sequence = iteration_sequence,
      user_id = user_id,
      filter_id = token
    ).first()

    print(f"TOKEN: {token}; original_record: {original_record}")
    # New Record
    if not original_record:
      print("inside if block")
      original_record = Iterations(
        iteration_sequence=iteration_sequence,
        user_id=user_id,
        task_number=1,
        time=int(time),
        user_choice=user_input.get('imageInfo').get('id'),
        filter_id=token
      )
      
      session.add(original_record)
      session.flush()

      verify_record = session.query(Iterations).filter_by(
        iteration_sequence = iteration_sequence,
        user_id = user_id,
        filter_id = token
      ).first()

      print(f"ADD & FLUSH: {verify_record.id}; {verify_record.id == original_record.id}")
      sys.stdout.flush()
    else:
      print(f"ITERATION: existing record: {original_record.task_number}")
      sys.stdout.flush()
      original_record.time = int(time)
      original_record.user_choice = user_input.get('imageInfo').get('id')
      session.flush()
 
    print(f"begin distractions... {original_record.id}")
    distractions = transform_into_distraction_records(distraction_list, original_record)
    sys.stdout.flush()
    session.add_all(distractions)
    print("end distractions...")
    sys.stdout.flush()

    session.commit()
  except Exception as error:
    error_str = str(error)

    print(f"error: {error_str}")
    sys.stdout.flush()
    return error_str

  return jsonify({
    "token": "Test success"
  })

@iteration_bp.route('/comparison/', methods=["POST"])
def save_iteration_comparison():
  req = request.get_json()
  iteration_sequence = int(req["iterationId"])
  token = int(req["token"])
  user_id = int(req["userId"])
  time = int(req["time"])
  user_input = int(req["userInput"])
  task_number = req["taskNumber"]
  print(f"COMPARISON - {iteration_sequence}")

  try:
    session = get_session(pg_engine)
    # get comparison images and get the number based on the index
    selected_comparison_image = comparison_image_id.get(f"{iteration_sequence}")[user_input]
    image_info = int(selected_comparison_image.split(" ")[1])
    print(f"COMPARISON - image_info: {image_info}")

    iteration_result = Iterations(
      iteration_sequence=iteration_sequence,
      user_id=user_id,
      filter_id=token,
      task_number=2,
      time=time,
      user_choice=image_info
    )
    print(f"COMPARISON - iteration_result: {iteration_result}")
    sys.stdout.flush()

    session.add(iteration_result)
    session.commit()
  except Exception as error:
    error = str(error)
    return error

  return jsonify({
    "token": "Test success"
  })

@iteration_bp.route('/latest_iteration/', methods=["GET"])
def get_iteration():
  # need user id
  token = request.args.get("token")
  user_id = request.args.get("userId")
 
  try:
    session = get_session(pg_engine)
    iterations = session.query(Iterations).filter(
      Iterations.filter_id == token,
      Iterations.user_id == user_id,
      Iterations.user_choice.is_not(None)
    ).order_by(Iterations.iteration_sequence).all()

    # Last record that has a response/answer
    # TODO: REVISIT THIS LOGIC
    # if iterations != None and len(iterations) > 0 and iterations[len(iterations) - 1].user_choice:
    print(f"iterations length: {len(iterations)}")
    # print(f"iteration in latest | choice: {iterations[len(iterations) - 1].user_choice}, user_id: {iterations[len(iterations) - 1].user_id}, filter: {iterations[len(iterations) - 1].filter_id}, sequence: {iterations[len(iterations) - 1].iteration_sequence}")
    #   latest_iteration = calculate_latest_iteration(iterations, False)
    # else:
    latest_iteration = calculate_latest_iteration(iterations, False)

    print(f"LATEST - latest: {latest_iteration}")
    sys.stdout.flush()
    return jsonify({
      "iteration": latest_iteration,
      "success": True,
      "token": token,
      "user_id": user_id
    })
  except Exception as error:
    print(str(error))
    return jsonify({"error": error})


def transform_into_distraction_records(distraction_list, iteration_record):
  mapped_distractions = list(map(lambda distraction_record: Distractions(
    iteration_id=iteration_record.id,
    distraction_from_start_time=int(distraction_record['distractionTimeFromStart']),
    distraction_from_current_timer=int(distraction_record['currentDistractionTime']),
    timer=int(distraction_record['timer']),
    user_id=iteration_record.user_id,
  ), distraction_list))
  return mapped_distractions