import sys
from flask import Blueprint, request, jsonify
from api.pg_schemas.iterations import Iterations
from api.pg_schemas.survey import Survey
from api.db import pg_engine, get_session

survey_bp = Blueprint('survey', __name__, )

@survey_bp.route('/user-survey-design/', methods=["POST"])
def save_design():
  try:
    req = request.get_json()
    user = int(req["userId"])
    token = int(req["token"])
    iteration_sequence = int(req["iterationId"])
    tlx = req["tlx"]
    task_number = req["taskNumber"]

    session = get_session(pg_engine)

    iteration_record = session.query(Iterations).filter_by(iteration_sequence = iteration_sequence, user_id = user, filter_id = token).first()
    # if tlx, do that, if comparison, do that
    tlx_results = transform_into_tlx(user, 'tlx', tlx, task_number=1, iteration_record=iteration_record)
    
    # comparison_results = transform_into_comparison(user, iteration, 'comparison', comparison)
    # new_survey_record.response = comparison_results

    session.bulk_save_objects(tlx_results)
    session.commit()
  except Exception as error:
    str_error = str(error) # unsure about this
    print(f"Error saving survey: {str_error}")
    return str_error

  return jsonify({
    "success": True
  })


@survey_bp.route('/user-survey-visual/', methods=["POST"])
def save_visual_dissimilarity():
  req = request.get_json()
  iteration_sequence = int(req["iterationId"])
  task_number = req["taskNumber"]
  tlx = req["tlx"]
  user_id = int(req["userId"])
  token = int(req["token"])

  try: 
    session = get_session(pg_engine)
    iteration_record = session.query(Iterations).filter_by(iteration_sequence = iteration_sequence, user_id = user_id, filter_id = token).first()
    # comparison_results = transform_into_comparison(user, iteration, 'comparison', comparison)
    tlx_results = transform_into_tlx(user_id, 'tlx', tlx, task_number=2, iteration_record=iteration_record)

    session.bulk_save_objects(tlx_results)
    session.commit()
  except Exception as error:
    error = str(error) # unsure about thiWes
    return error

  return jsonify({
    "success": True
  })

@survey_bp.route('/pairwise-comparison/', methods=["POST"])
def save_pairwise_comparison():
  req = request.get_json()
  user_id = int(req["userId"])
  token = int(req["token"])
  iteration_sequence = int(req["iterationId"])
  task_number = req["taskNumber"]
  comparison = req["comparison"]

  try:
    session = get_session(pg_engine)
    print(f"iteration_sequence: {iteration_sequence} task_number:{task_number}")
    sys.stdout.flush()
    # TODO: get token
    iteration_record = session.query(Iterations).filter_by(iteration_sequence = iteration_sequence, user_id = user_id, filter_id = token).first()
    comparison_results = transform_into_comparison(user_id, iteration_record, 'comparison', comparison, task_number=3)

    print(f"pairwise things {comparison_results[0].task_number} {comparison_results[0].question}")
    sys.stdout.flush()
    # tlx_results = transform_into_c(user_id, iteration_id, 'tlx', tlx, task_number=2)

    session.bulk_save_objects(comparison_results)
    session.commit()
  except Exception as error:
    error = str(error) # unsure about this
    return error

  return jsonify({
    "success": True
  })


def transform_into_comparison(user_id, iteration_record, survey_type, results, task_number):
  mapped_comparison_results = list(map(lambda survey_record: Survey(user_id=user_id, iteration_id=iteration_record.id, survey_type=survey_type, question=int(survey_record.get("question")), response=int(survey_record.get("response")), task_number=task_number), results))
  return mapped_comparison_results

def transform_into_tlx(user_id, survey_type, tlx_results, task_number, iteration_record):
  mapped_tlx_results = list(map(lambda tlx_record: Survey(
    user_id=user_id,
    iteration_id=iteration_record.id,
    survey_type=survey_type,
    question=int(tlx_record.get("question")) + 1 ,
    response=int(tlx_record.get("response")),
    task_number=task_number
  ), tlx_results))
  return mapped_tlx_results