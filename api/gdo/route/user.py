import sys
import uuid
from flask import Blueprint, request, jsonify
from api.pg_schemas.users import Users
from api.pg_schemas.user_filter import UserFilter
from api.pg_schemas.iterations import Iterations
from api.db import pg_engine, get_session
from api.helpers.helpers import calculate_latest_iteration


user_bp = Blueprint('user', __name__, )

@user_bp.route('/welcome')
def hello_world():
  return "<h1>Welcome to the Generate Design Study.</h1>"

@user_bp.route('/signin/', methods=["POST"])
def sign_in():
  req = request.get_json()
  token = parse_token(req.get("userToken", ""))
  username = req.get("user", "")
  uid = None
  iteration = None

  if not is_valid_token(token):
    return jsonify({"error": "Token is not valid."})
  if not username:
    return jsonify({"error": "User name is not valid."})

  # with Session.begin() as session:
  try:
    session = get_session(pg_engine)
    first_and_last = username.split()

    # TODO: middle name logic or spaces in last name
    if len(first_and_last) != 2:
      raise Exception('Invalid User')

    user = session.query(Users).filter(
      Users.first_name == first_and_last[0],
      Users.last_name == first_and_last[1]
    ).first()
    uid = user.id if (user and user.id) else None

    user_filter = None
    if uid:
      user_filter = session.query(UserFilter).filter(
        UserFilter.filter_id == token,
        UserFilter.user_id == uid
      ).first()

    print(f"user_filter: {user_filter}")

    if uid != None and user_filter != None:
      # Attempt to get latest iteration based on user_choice
      iteration = session.query(
        Iterations
      ).filter(
        Iterations.filter_id == user_filter.filter_id,
        Iterations.user_id == uid,
        Iterations.user_choice.is_not(None)
      ).order_by(Iterations.iteration_sequence).all()
    
    print(f"continue forward to user check and token check")

    if user is None:
      user = Users(first_name=first_and_last[0], last_name=first_and_last[1])
      session.add(user)
      session.flush()
      uid = user.id
      print(f"newly created user: {user.first_name}")

    if user_filter is None:
      user_filter = UserFilter(filter_id = token, user_id = uid)
      session.add(user_filter)
      session.flush()
      print(f"newly created filter: {user_filter.filter_id}")
    
    latest_iteration = calculate_latest_iteration(iteration, True)
    print(f"/users latest_iteration: {latest_iteration}")

    if latest_iteration == -1:
      raise Exception("No more iterations to complete for this filter.")
    
    # TODO: remove creation of iteration]
    # created_iteration = Iterations(
    #   iteration_sequence=latest_iteration,
    #   filter_id = token,
    #   user_id=uid,
    #   task_number=1,
    #   user_choice="",
    #   time=0
    # )
    # print(f"iteration: {created_iteration}")
    # session.add(created_iteration)

    # Commit all changes to session
    session.commit()

  except Exception as error:
    print(str(error))
    # TODO: session.rollback()
    return jsonify({"error": error})
  else:
    # TODO
    print(f"Try/Catch/Else")

  print(f"Pre Response Return")
  return jsonify({
    "token": token,
    "userId": uid,
    "success": True
  })
    
def is_valid_token(token):
  if token is None:
    return False
  else:
    return not (token > 7 or token < 1)

def parse_token(user_token):
  if user_token == None or user_token == "":
    return None
  else:
    return int(user_token)