from inspect import trace
import os, base64, io, glob, sys
from flask import Blueprint, Response, jsonify, request
from PIL import Image
from api.gdo.csv_service.csv_service import CsvService
from api.comparison_images.comparison_images_by_iteration import comparison_image_id, sort_images_by_iteration, comparison_image_id_with_filter, get_filtered_list

# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# STATIC_URL = "/static"
# MEDIA_URL = "/media"
# MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'static')
# CORS_WHITELIST = "localhost:3000"


image_bp = Blueprint('image', __name__, )
# base_path = '../api/static'
# base_path = os.path.abspath(os.path.join('api', os.pardir, os.pardir, os.pardir, 'static'))
base_path = os.path.abspath(os.path.join(os.getcwd(), 'api/static'))


@image_bp.route('/iteration_images/', methods=["GET"])
def get_iteration_images():
  try:
      # base_path = '../api/static'
    iteration = int(request.args.get("iteration"))
    token_filter = int(request.args.get("token"))

    csvService = CsvService(iteration)
    data = csvService.read_csv()

    sorted_data = sort_images_by_iteration(token_filter, data)
    
    encoded_img = build_images_array(base_path, sorted_data)
    # print the data and confirm
    
    # filtered_list = list(map(lambda image: build_response_image(image), glob.iglob(full_dir)))
    # for row in data:
    #   row["img_data"] = encoded_img[row.get("OutcomeID")]

    # IS THIS SORTING?
    return jsonify({
      "images": encoded_img,
      "imageData": sorted_data,
      "status": "Success"
    })
  except Exception as err: 
    return Response(f"Error retrieving images: {err}")

def build_images_array(image_dir, csv_data):
  full_dir = image_dir + '/*'
  # logger.debug(f"Full Dir: {full_dir}")

  # filter by iteration ID here! sorted csv_data
  files_names = [image for image in glob.iglob(full_dir)]
  filtered_files = [file_name for file_name in files_names for row in csv_data if f'Outcome {row.get("OutcomeID")} (' in file_name ]
  final = list(map(lambda image: build_response_image(image), filtered_files))
  return final

def build_response_image(image):
  pil_img = Image.open(image, mode='r') # reads the PIL image
  byte_arr = io.BytesIO()
  pil_img.save(byte_arr, format='JPEG') # convert the PIL image to byte array
  encoded_img = base64.encodebytes(byte_arr.getvalue()).decode('ascii') # encode as base64
  return encoded_img


# TODO: Add filter bits with iteration using token
@image_bp.route('/comparison_images/', methods=["GET"])
def get_comparison_images():
  try:
    iteration = int(request.args.get("iteration"))
    token_filter = int(request.args.get("token"))

    comparison_files = get_comparison_jpg(base_path, iteration, token_filter)

    return jsonify({
      "images": comparison_files,
      "status": "Success"
    })
  except Exception as err: 
    return Response(f"Error retrieving images: {err}")


def get_comparison_jpg(image_dir, iteration, token_filter):
  
  # image_ids = comparison_image_id[f"{iteration}"]
  images = get_filtered_list(token_filter, iteration)

  # filenames = [f"{image_dir}/{image}.JPG" for image in image_ids]
  # comparison_globs = [glob.iglob(f"{full_dir}/{image}") for image in image_ids]
  filenames_with_filter = [f"{image_dir}/{image}.JPG" for image in images]

  comparison_images = list(map(lambda image: build_response_image(image), filenames_with_filter))
  return comparison_images