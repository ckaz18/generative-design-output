import os
import csv
import sys

class CsvService:
  def __init__(self, iteration):
    self.iteration = iteration
    self.ordered_list = []

  def get_file_by_iteration(self):
    if self.iteration == 1:
      return "Data1000.csv"
    if self.iteration == 2:
      return "Data500.csv"
    elif self.iteration == 3:
      return "Data250.csv"
    elif self.iteration == 4:
      return "Data100.csv"
    elif self.iteration == 5:
      return "Data50.csv"
    elif self.iteration == 6:
      return "Data25.csv"
    elif self.iteration == 7:
      return "Data10.csv"
    else:
      return None

  def read_csv(self):
    file_name = self.get_file_by_iteration()

    base_path = os.path.abspath(os.path.join(os.getcwd(), 'api/csv'))
    # print(f"Base_path: {base_path}")
    # sys.stdout.flush()

    if file_name:
      with open(f"{base_path}/{file_name}", newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
          self.ordered_list.append(row)
      return self.ordered_list
    else:
      return None