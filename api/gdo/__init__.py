# from flask import Flask
# from flask_cors import CORS
# from flask_injector import FlaskInjector
# from injector import (Injector)

# from gdo.config import DevelopmentConfig
# from gdo.route.user import user_bp
# from gdo.socketio import socketio_config
# # from gdo.model.models import db, migrate, marshmallow
# # from gdo.route.swagger import swagger_bp
# # from gdo.route.swagger import swaggerui_blueprint


# INJECTOR_DEFAULT_MODULES = dict(
# )


# def _configure_dependency_injection(flask_app, injector_modules, custom_injector) -> None:
#     modules = dict(INJECTOR_DEFAULT_MODULES)

#     if injector_modules:
#         modules.update(injector_modules)

#     FlaskInjector(
#         app=flask_app,
#         injector=custom_injector,
#         modules=modules.values(),
#     )


# def register_blueprints(app, blueprints):
#     for blueprint in blueprints:
#         app.register_blueprint(blueprint)
#     # app.register_blueprint(swaggerui_blueprint, url_prefix='/swagger')


# def create_app(*, config_class=DevelopmentConfig, custom_injector: Injector = None, injector_modules=None):
#     app = Flask(__name__)
#     app.config.from_object(config_class)

#     with app.app_context():
#         CORS(app)
#         # db.init_app(app)
#         # marshmallow.init_app(app)
#         # migrate.init_app(app, db)
#         register_blueprints(app, [user_bp])
#         _configure_dependency_injection(app, injector_modules, custom_injector)
#     return app


# application = create_app()
# socketio = socketio_config(application)


# if __name__ == '__main__':
#     socketio.run()
