from api.db import Base
from sqlalchemy import Column, ForeignKey, Integer


class UserFilter(Base):
  __tablename__ = 'gdo_user_filter'
  filter_id = Column(Integer, primary_key = True)
  user_id = Column(Integer, ForeignKey("gdo_users.id"), primary_key = True)
