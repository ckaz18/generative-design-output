from api.db import Base
from sqlalchemy import Column, ForeignKeyConstraint, Integer, String
from sqlalchemy.sql.schema import ForeignKey
from api.pg_schemas.iterations import Iterations


class Survey(Base):
  __tablename__ = 'survey'
  survey_id = Column(Integer, primary_key = True)
  user_id = Column(Integer, ForeignKey("gdo_users.id"))
  iteration_id = Column(Integer, ForeignKey("iterations.id"))
  task_number = Column(Integer)
  survey_type = Column(String)
  question = Column(Integer)
  response = Column(String)
  __table_args__ = (
    ForeignKeyConstraint([iteration_id, user_id], [Iterations.id, Iterations.user_id]),
    {},  
  )
