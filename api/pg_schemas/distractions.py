from sqlalchemy.sql.schema import ForeignKey
from api.db import Base
from sqlalchemy import Column, ForeignKeyConstraint, Integer
from sqlalchemy.sql.schema import ForeignKey
from api.pg_schemas.iterations import Iterations

class Distractions(Base):
  __tablename__ = 'distractions'
  id = Column(Integer, primary_key = True)
  user_id = Column(Integer, ForeignKey("gdo_users.id"))
  distraction_from_start_time = Column(Integer)
  distraction_from_current_timer = Column(Integer)
  timer = Column(Integer)
  iteration_id = Column(Integer)
  __table_args__ = (
    ForeignKeyConstraint([iteration_id, user_id], [Iterations.id, Iterations.user_id]),
    {},  
  )