from api.db import Base
# from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, UniqueConstraint
from sqlalchemy.dialects.postgresql import UUID


class Users(Base):
  __tablename__ = 'gdo_users'
  id = Column(Integer, primary_key = True)
  first_name = Column(String)
  last_name = Column(String)
