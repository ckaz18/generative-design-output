from sqlalchemy.sql.schema import ForeignKey
from api.db import Base
from sqlalchemy import Column, ForeignKeyConstraint, Integer, String
from sqlalchemy.sql.schema import ForeignKey
from api.pg_schemas.user_filter import UserFilter

class Iterations(Base):
  __tablename__ = 'iterations'
  id = Column(Integer, primary_key = True)
  filter_id = Column(Integer)
  iteration_sequence = Column(Integer)
  user_id = Column(Integer, ForeignKey("gdo_users.id"))
  task_number = Column(Integer)
  user_choice = Column(String)
  time = Column(Integer)
  __table_args__ = (
    ForeignKeyConstraint([filter_id, user_id], [UserFilter.filter_id, UserFilter.user_id]),
    {},
  )
