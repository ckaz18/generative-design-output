
-- GDO_USERS
-- TODO: user_token should not be unique

CREATE TABLE public.gdo_users (
    user_id integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    user_token uuid UNIQUE,
    first_name character varying,
    last_name character varying
);


-- USERS
-- TODO: Remove this table; it's trash

CREATE SEQUENCE public.users_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.users (
    user_id integer PRIMARY KEY DEFAULT NEXTVAL('users_user_id_seq'),
    user_token uuid,
    first_name character varying,
    last_name character varying
);


-- ITERATIONS
-- TODO: remove sequence from iteration_id

CREATE SEQUENCE public.iterations_iteration_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.iterations (
    iteration_id integer NOT NULL DEFAULT NEXTVAL('iterations_iteration_id_seq'),
    user_id integer REFERENCES gdo_users(user_id),
    task_number integer,
    user_choice character varying,
    "time" integer,
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    CONSTRAINT iteration_id_ukey UNIQUE(id)
);



-- DISTRACTIONS

CREATE TABLE public.distractions (
    id integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    timer integer,
    distraction_from_start_time bigint,
    distraction_from_current_timer bigint,
    iteration_id integer NOT NULL
);

ALTER TABLE ONLY public.distractions
    ADD CONSTRAINT iterations_id_fkey FOREIGN KEY (iteration_id) REFERENCES public.iterations(id) ON DELETE CASCADE;



-- SURVEY
-- TODO: user_id should reference gdo_users | add foreign key to iteration_id

CREATE TABLE public.survey (
    survey_id integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    user_id integer REFERENCES users(user_id),
    iteration_id integer,
    task_number integer,
    survey_type character varying,
    question integer,
    response character varying
);
