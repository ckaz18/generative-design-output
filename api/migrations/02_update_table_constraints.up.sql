
ALTER TABLE iterations DROP CONSTRAINT iterations_user_id_fkey;
ALTER TABLE survey DROP CONSTRAINT survey_user_id_fkey;
ALTER TABLE gdo_users DROP COLUMN user_token;
ALTER TABLE gdo_users RENAME COLUMN user_id TO id;

CREATE TABLE gdo_user_filter (
  filter_id integer NOT NULL,
  user_id integer REFERENCES gdo_users(id),
  PRIMARY KEY (filter_id, user_id)
);


DROP TABLE users;


ALTER TABLE distractions DROP CONSTRAINT iterations_id_fkey;
ALTER TABLE iterations ALTER COLUMN iteration_id DROP DEFAULT;
DROP SEQUENCE iterations_iteration_id_seq;
ALTER TABLE iterations ADD CONSTRAINT iterations_pkey PRIMARY KEY (id);  
ALTER TABLE iterations ADD COLUMN filter_id integer NOT NULL;
ALTER TABLE iterations ADD CONSTRAINT iterations_gdo_user_filter_fkey FOREIGN KEY (filter_id, user_id) REFERENCES gdo_user_filter(filter_id, user_id);
ALTER TABLE iterations ADD CONSTRAINT iterations_id_user_id_unique UNIQUE (id, user_id);


ALTER TABLE survey ADD CONSTRAINT survey_user_id_fkey FOREIGN KEY (user_id) REFERENCES gdo_users(id);
ALTER TABLE survey ADD CONSTRAINT survey_iteration_id_fkey FOREIGN KEY (iteration_id, user_id) REFERENCES iterations(id, user_id);

ALTER TABLE distractions ADD COLUMN user_id integer NOT NULL;
ALTER TABLE distractions ADD CONSTRAINT distractions_user_id_fkey FOREIGN KEY (user_id) REFERENCES gdo_users(id);
ALTER TABLE distractions ADD CONSTRAINT distractions_iteration_id_fkey FOREIGN KEY (iteration_id, user_id) REFERENCES iterations(id, user_id);
