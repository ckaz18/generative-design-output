# generative-design-output

Initial generative design output experiment

# Setup
## NodeJS
1. Install [NodeJS](https://nodejs.org/en/download/) version 12.X for your machine
1. Go through installer wizard and Finish
1. Verify the installation
  
    - `node -v`
1. Check that NPM has also been installed with your NodeJS installation

  - `npm -v`


## Python 3.8.10
1. This guide goes over basic installation for (Python)[https://realpython.com/installing-python/]
1. Windows installer 64-bit for (Python 3.8.10)[https://www.python.org/ftp/python/3.8.10/python-3.8.10-amd64.exe]
1. Verify the installation

    - `python` or `python3` or `python --version`
1. Verify that pip was also installed

  - `pip3`

## Future Features
- `release: python manage.py migrate`