POSTGRES_DIR ?= $(PWD)/postgres-data

PG_VERSION := 13.8
DATABASE_URL ?= postgresql://postgres@gd_db:5432/postgres?sslmode=disable

.PHONY: run-db
run-db: docker-net
	mkdir -p $(POSTGRES_DIR)
	docker run --detach \
		--network gdo \
		--env POSTGRES_HOST_AUTH_METHOD=trust \
		--user $(uid):1000 \
		--volume $(POSTGRES_DIR):/var/lib/postgresql/data/pgdata \
		--env PGDATA=/var/lib/postgresql/data/pgdata \
		--name gd_db postgres:$(PG_VERSION)
	for i in $(shell seq 1 30); \
	do \
		if docker exec gd_db psql -U postgres --list; \
		then \
			break; \
		fi; \
		sleep 1; \
	done

.PHONY: build-web
build-web:
	docker build . --tag=gd_api --file Dockerfile.web

.PHONY: run-web
run-web: build-web run-db migrate
	docker run --network gdo --env DATABASE_URL=$(DATABASE_URL) --env PORT=80 --publish 5050:80 --name gd_api gd_api

.PHONY: docker-net
docker-net:
	-docker network create gdo 2>&1

.PHONY: clean
clean:
	-docker rm -f gd_db gd_api

.PHONY: clean-data
clean-data:
	-rm -rf $(POSTGRES_DIR)

.PHONY: build-migrate
build-migrate:
	cd ./api/migrations && docker build . --tag=gd_migrations --file Dockerfile.release

.PHONY: migrate
migrate: build-migrate run-db
	docker run --rm --network gdo --env DATABASE_URL=$(DATABASE_URL) --name gd_migrations gd_migrations

.PHONY: push
push:
	heroku container:push --recursive --app generative-design-study

.PHONY: release
release: push
	heroku container:release web release --app generative-design-study
